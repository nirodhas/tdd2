/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package llist;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nirodha
 */
public class LlistTest {
    
    public LlistTest() {
        
    }
    
    public Llist getList(){
        
        Llist list = new Llist();
        
        for(int i = 0 ; i < 10 ; i++){
            if(i%2==0){
                list.addTail(2*i);
            }else{
            list.addHead(i);
            }
        }
        
        return list;
    }

    /**
     * Test of addTail method, of class Llist.
     */
    @Test
    public void testAddTail_int() {
        Llist llist = new Llist();
        llist.addTail(5);
        assertEquals(llist.tail.data(),5);
    }

    /**
     * Test of addHead method, of class Llist.
     */
    @Test
    public void testAddHead() {
        Llist llist = new Llist();
        llist.addTail(5);
        llist.addHead(100);
        llist.addTail(2);
        
        assertEquals(llist.head.data(),100);
    }

    /**
     * Test of delTail method, of class Llist.
     */
    @Test
    public void testDelTail() {
        Llist llist = new Llist();
        llist.addTail(5);
        llist.addHead(100);
        llist.addTail(2);
        
        llist.delTail();
        assertEquals(llist.tail.data(),5);
    }

    /**
     * Test of size method, of class Llist.
     */
    @Test
    public void testSize() {
        
        Llist llist = new Llist();
        llist.addTail(5);
        llist.addHead(100);
        llist.addTail(2);
        
        assertEquals(llist.size(),3);
    }

    /**
     * Test of delHead method, of class Llist.
     */
    @Test
    public void testDelHead() {
        Llist llist = new Llist();
        llist.addTail(5);
        llist.addHead(100);
        llist.addTail(2);
        //deletes the head
        llist.delHead();
        
        assertEquals(llist.head.data(),5);
    }

    /**
     * Test of delAll method, of class Llist.
     */
    @Test
    public void testDelAll() {
        LlistTest lltest = new LlistTest();
        Llist newList = lltest.getList();
        //creates a list with 10 nodes
        newList.delAll(4);
        assertEquals(newList.size(),9);
        
    }

    /**
     * Test of sum method, of class Llist.
     */
    @Test
    public void testSum() {
        
        LlistTest lltest = new LlistTest();
        Llist newList = lltest.getList();
        //creates a list with 10 nodes
        
        assertEquals(newList.sum(),65);
    }

    /**
     * Test of sumEven method, of class Llist.
     */
    @Test
    public void testSumEven() {
        LlistTest lltest = new LlistTest();
        Llist newList = lltest.getList();
        //creates a list with 10 nodes
        
        assertEquals(newList.sumEven(),65);
    }

    /**
     * Test of count method, of class Llist.
     */
    @Test
    public void testCount() {
    }

    /**
     * Test of addTail method, of class Llist.
     */
    
    
}
