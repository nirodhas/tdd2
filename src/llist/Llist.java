/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package llist;

/**
 *
 * @author Nirodha
 */

import java.util.*;
public class Llist {
    Node head = null;
    Node tail = null; 
	//empty list
	public void addTail(int x){ //add at head
		Node nw = new Node(x);
		if(head == null){
			head = nw; tail = nw;
		}
		else{
	    tail.setNext(nw);
		  tail = nw;
		}
	}

  
	public void addHead(int x){
		Node tmp = head;
		Node newNode = new Node(x);

		head = newNode;
		head.setNext(tmp) ;
		  	//insert x at head of list

	 }
  
	public void delTail(){
		Node n = head;

		while(n.next()!=tail){
			n = n.next();
		}

		n.setNext(null);
		tail = n;
	//remove element at tail of list
	}
 
	public int size(){

		Node n = head;
		int count = 0;
		while(n!=null){
			n = n.next();
			count++;
		}

		return count;
		//return number of nodes in the list
	}
	public void delHead(){

		Node tmp = head;
		head = head.next();

    //remove element at head of list
	}
	public void delAll(int x){

		Node k = head;
		Node bk = head;
		
		while(k != null){
			if(k.data() == x){
				if(k == head)
					head = k.next();
				else if(k == tail){
			    	bk.setNext(null);
			    	tail = bk;
			    }else
				bk.setNext(k.next());		
			}
			bk = k; 
			k = k.next();
		}
		
		//delete all occurrences of x from the list
	}
	public int sum(){
		Node k = head;
		int sumVal = 0;
		while(k!=null){
			sumVal = sumVal + k.data();

			k = k.next();
		}

		return  sumVal;
		//calculate sum of values in the list
	}
	public int sumEven(){
		Node k = head;
		int sumValEven = 0;
		while(k!=null){
			if (k.data()%2==0) {
				sumValEven = sumValEven + k.data();	
			}
			

			k = k.next();
		}

		return  sumValEven;
		//calculate sum of even values in the list
	}
	public int count(int x){

		Node k = head;
		int ocCount = 0;

		while (k!=null) {
			if (k.data()==x) {
				ocCount++;
			}
			k = k.next();
		}

		return ocCount;
		//count number of occurrences of x in list
	}
	public void addTail(Llist lst){
		tail.setNext(lst.head);
		tail = lst.tail;
    	//add given list to tail of existing list
  	} 
  	
	public class Node{
		int data;
		Node next;
		public Node(int x){
			data = x; next = null;
		}

		public Node next(){
			return next;
		}

		public void setNext(Node p){
			next = p;
		}

		public void set(int x){
			data = x;
		}
		public int data(){
			return data;
		}
	}
}
